﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Downtime.Models
{
    public class PowerPlanItems
    {
        public string Name { get; set; }
        public string ServiceLineValue { get; set; }
        public int ID { get; set; }
    }
}