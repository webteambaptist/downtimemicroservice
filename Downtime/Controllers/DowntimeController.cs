﻿using Downtime.Models;
using Microsoft.SharePoint.Client;
using NLog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Services.Client;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;

namespace Downtime.Controllers
{
    [RoutePrefix("api/downtime")]
    public class DowntimeController : ApiController
    {
        private static readonly Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        private static readonly string DowntimePlanUrl = ConfigurationManager.AppSettings["DowntimePlanUrl"].ToString();
        private static readonly string DowntimePlanPDFUrl = ConfigurationManager.AppSettings["DowntimePlanPDFUrl"].ToString();
        private static readonly string AdultsGuid = ConfigurationManager.AppSettings["AdultsGuid"].ToString();
        private static readonly string PedsGuid = ConfigurationManager.AppSettings["PedsGuid"].ToString();

        public DowntimeController()
        {
            var config = new NLog.Config.LoggingConfiguration();
            var logFile = new NLog.Targets.FileTarget("logfile") { FileName = $"Logs\\DowntimeMicroservice{DateTime.Today:MM-dd-yy}.log" };
            config.AddRule(LogLevel.Info, LogLevel.Fatal, logFile);
            NLog.LogManager.Configuration = config;
        }

        // Original path in PPAPI DowntimeForms2016PowerPlansAdults
        [HttpGet]
        [Route("DowntimePowerPlansAdults")]
        public IHttpActionResult GetDowntimePowerPlansAdults()
        {
            var powerPlansAdults = new PowerPlansAdults
            {
                PowerPlansAdultsItem = new List<PowerPlanItems>()
            };

            try
            {
                using (var ctx = new ClientContext(DowntimePlanUrl))
                {
                    ctx.Credentials = new NetworkCredential("wedadmsvc", "w3d4DM$vC", "bh");
                    var list = ctx.Web.Lists.GetById(new Guid(AdultsGuid));

                    var view = list.Views.GetByTitle("All Documents");
                    ctx.Load(view);

                    var query = new CamlQuery();
                    if (ctx.HasPendingRequest)
                    {
                        ctx.ExecuteQuery();
                    }

                    query.ViewXml = $"<View><Query>{view.ViewQuery}{query}</Query></View>";

                    var items = list.GetItems(query);

                    ctx.Load(items);

                    if (ctx.HasPendingRequest)
                    {
                        ctx.ExecuteQuery();
                    }


                    foreach (var item in items)
                    {
                        var powerPlansItems = new PowerPlanItems
                        {
                            ID = item.Id,
                            Name = item["FileLeafRef"].ToString().Replace(".pdf", ""),
                            ServiceLineValue = item["Service_x0020_Line"].ToString()
                        };

                        powerPlansAdults.PowerPlansAdultsItem.Add(powerPlansItems);
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Error("Exception occurred getting Downtime Plans Adults :: " + e.Message);
                return BadRequest();
            }

            return Ok(powerPlansAdults);
        }

        // Original path in PPAPI DowntimeForms2016PowerPlansPeds
        [HttpGet]
        [Route("DowntimePowerPlansPeds")]
        public IHttpActionResult GetDowntimePowerPlansPeds()
        {
            var powerPlansPeds = new PowerPlansPeds
            {
                PowerPlansPedsItem = new List<PowerPlanItems>()
            };

            try
            {
                using (var ctx = new ClientContext(DowntimePlanUrl))
                {
                    ctx.Credentials = new NetworkCredential("wedadmsvc", "w3d4DM$vC", "bh");
                    var list = ctx.Web.Lists.GetById(new Guid(PedsGuid));

                    var view = list.Views.GetByTitle("All Documents");
                    ctx.Load(view);

                    var query = new CamlQuery();
                    if (ctx.HasPendingRequest)
                    {
                        ctx.ExecuteQuery();
                    }

                    query.ViewXml = $"<View><Query>{view.ViewQuery}{query}</Query></View>";

                    var items = list.GetItems(query);

                    ctx.Load(items);

                    if (ctx.HasPendingRequest)
                    {
                        ctx.ExecuteQuery();
                    }

                    foreach (var item in items)
                    {
                        var powerPlansItems = new PowerPlanItems
                        {
                            ID = item.Id,
                            Name = item["FileLeafRef"].ToString().Replace(".pdf", ""),
                            ServiceLineValue = item["Service_x0020_Line"].ToString()
                        };

                        powerPlansPeds.PowerPlansPedsItem.Add(powerPlansItems);
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Error("Exception occurred getting Downtime Plans Peds :: " + e.Message);
                return BadRequest();
            }

            return Ok(powerPlansPeds);
        }

        // Original path in PPAPI DowntimeForms2016PowerPlansAdultsByID
        [HttpPost]
        [Route("DowntimePowerPlansAdultsByID")]
        public IHttpActionResult GetDowntimeFormsPowerPlansAdultsByID()
        {
            try
            {
                var headers = Request.Headers;
                string linkId;

               var doc = new DowntimePlans2016SVC.DowntimeFormsPowerPlansAdultsItem();

                var pp = new DowntimePlans2016SVC.InformaticsDataContext(new Uri(DowntimePlanPDFUrl))
                {
                    Credentials = CredentialCache.DefaultCredentials
                };

                var documentList = new List<DowntimePlans2016SVC.DowntimeFormsPowerPlansAdultsItem>();
                documentList = pp.DowntimeFormsPowerPlansAdults.ToList();

                if (headers.Contains("ID"))
                {
                    linkId = headers.GetValues("ID").First();
                    doc = (from x in documentList where x.Id.ToString() == linkId select x).First();
                }
                else
                {
                    Logger.Error("Exception occurred getting DowntimePowerPlansAdultsByID HEADER DID NOT INCLUDE (ID) ");
                    return BadRequest();
                }

                var dssr = pp.GetReadStream(doc);
                var stream = dssr.Stream;

                var result = new HttpResponseMessage(HttpStatusCode.OK)
                {
                    Content = new StreamContent(stream)
                };

                result.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment")
                {
                    FileName = doc.Name
                };

                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");

                var response = ResponseMessage(result);

                return response;
            }
            catch (Exception ex)
            {
                Logger.Error("Exception occurred getting id/object  from DowntimePowerPlansAdultsByID :: " + ex.Message);
                return BadRequest();
            }
        }

        // Original path in PPAPI DowntimeForms2016PowerPlansPedsByID
        [HttpPost]
        [Route("DowntimePowerPlansPedsByID")]
        public IHttpActionResult GetDowntimePowerPlansPedsByID()
        {
            try
            {
                var headers = Request.Headers;
                string linkId;

                var doc = new DowntimePlans2016SVC.DowntimeFormsPowerPlansPedsItem();

                var pp = new DowntimePlans2016SVC.InformaticsDataContext(new Uri(DowntimePlanPDFUrl))
                {
                    Credentials = CredentialCache.DefaultCredentials
                };

                var documentList = new List<DowntimePlans2016SVC.DowntimeFormsPowerPlansPedsItem>();
                documentList = pp.DowntimeFormsPowerPlansPeds.ToList();

                if (headers.Contains("ID"))
                {
                    linkId = headers.GetValues("ID").First();
                    doc = (from x in documentList where x.Id.ToString() == linkId select x).First();
                }
                else
                {
                    Logger.Error("Exception occurred getting DowntimePowerPlansPedsByID HEADER DID NOT INCLUDE (ID) ");
                }

                var dssr = pp.GetReadStream(doc);
                var stream = dssr.Stream;

                var result = new HttpResponseMessage(HttpStatusCode.OK)
                {
                    Content = new StreamContent(stream)
                };

                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                {
                    FileName = doc.Name
                };

                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");

                var response = ResponseMessage(result);

                return response;
            }
            catch (Exception ex)
            {
                Logger.Error("Exception occurred getting id/object  from DowntimePowerPlansPedsByID :: " + ex.Message);
                return BadRequest();
            }
        }
    }
}
